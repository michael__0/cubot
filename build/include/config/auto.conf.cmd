deps_config := \
	/Users/liuming/esp/esp-adf/esp-idf/components/app_trace/Kconfig \
	/Users/liuming/esp/esp-adf/esp-idf/components/aws_iot/Kconfig \
	/Users/liuming/esp/esp-adf/esp-idf/components/bt/Kconfig \
	/Users/liuming/esp/esp-adf/esp-idf/components/driver/Kconfig \
	/Users/liuming/esp/esp-adf/esp-idf/components/esp32/Kconfig \
	/Users/liuming/esp/esp-adf/esp-idf/components/esp_adc_cal/Kconfig \
	/Users/liuming/esp/esp-adf/esp-idf/components/esp_event/Kconfig \
	/Users/liuming/esp/esp-adf/esp-idf/components/esp_http_client/Kconfig \
	/Users/liuming/esp/esp-adf/esp-idf/components/esp_http_server/Kconfig \
	/Users/liuming/esp/esp-adf/esp-idf/components/ethernet/Kconfig \
	/Users/liuming/esp/esp-adf/esp-idf/components/fatfs/Kconfig \
	/Users/liuming/esp/esp-adf/esp-idf/components/freemodbus/Kconfig \
	/Users/liuming/esp/esp-adf/esp-idf/components/freertos/Kconfig \
	/Users/liuming/esp/esp-adf/esp-idf/components/heap/Kconfig \
	/Users/liuming/esp/esp-adf/esp-idf/components/libsodium/Kconfig \
	/Users/liuming/esp/esp-adf/esp-idf/components/log/Kconfig \
	/Users/liuming/esp/esp-adf/esp-idf/components/lwip/Kconfig \
	/Users/liuming/esp/esp-adf/esp-idf/components/mbedtls/Kconfig \
	/Users/liuming/esp/esp-adf/esp-idf/components/mdns/Kconfig \
	/Users/liuming/esp/esp-adf/esp-idf/components/mqtt/Kconfig \
	/Users/liuming/esp/esp-adf/esp-idf/components/nvs_flash/Kconfig \
	/Users/liuming/esp/esp-adf/esp-idf/components/openssl/Kconfig \
	/Users/liuming/esp/esp-adf/esp-idf/components/pthread/Kconfig \
	/Users/liuming/esp/esp-adf/esp-idf/components/spi_flash/Kconfig \
	/Users/liuming/esp/esp-adf/esp-idf/components/spiffs/Kconfig \
	/Users/liuming/esp/esp-adf/esp-idf/components/tcpip_adapter/Kconfig \
	/Users/liuming/esp/esp-adf/esp-idf/components/vfs/Kconfig \
	/Users/liuming/esp/esp-adf/esp-idf/components/wear_levelling/Kconfig \
	/Users/liuming/esp/esp-adf/components/audio_board/Kconfig.projbuild \
	/Users/liuming/esp/esp-adf/esp-idf/components/bootloader/Kconfig.projbuild \
	/Users/liuming/esp/esp-adf/components/esp-adf-libs/Kconfig.projbuild \
	/Users/liuming/esp/esp-adf/esp-idf/components/esptool_py/Kconfig.projbuild \
	/Users/liuming/esp/cubot/main/Kconfig.projbuild \
	/Users/liuming/esp/esp-adf/esp-idf/components/partition_table/Kconfig.projbuild \
	/Users/liuming/esp/esp-adf/esp-idf/Kconfig

include/config/auto.conf: \
	$(deps_config)

ifneq "$(IDF_CMAKE)" "n"
include/config/auto.conf: FORCE
endif

$(deps_config): ;

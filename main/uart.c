
/* UART asynchronous example, that uses separate RX and TX tasks

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "esp_system.h"
#include "esp_log.h"
#include "include/audio_mem.h"
#include "driver/uart.h"
#include "soc/uart_struct.h"
#include "string.h"
#include "../include/main.h"
#include "../include/uart.h"

static const char *TAG = "UART";

//static const int RX_BUF_SIZE = 1024;
//static const int TX_BUF_SIZE = 1024;
#define RX_BUF_SIZE 1024
#define TX_BUF_SIZE 1024
#define TXD_PIN (GPIO_NUM_23)
#define RXD_PIN (GPIO_NUM_22)

typedef struct uart_tx_data_format
{
    int cmd;
    char *data;
    int len;
    char *buffer;
    int trans_len;
}uart_tx_data_format_t;

uart_tx_data_format_t tx_data;


void init(void)
{
    const uart_config_t uart_config = 
    {
        .baud_rate = 115200,
        .data_bits = UART_DATA_8_BITS,
        .parity = UART_PARITY_DISABLE,
        .stop_bits = UART_STOP_BITS_1,
        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE
    };
    uart_param_config(UART_NUM_1, &uart_config);
    uart_set_pin(UART_NUM_1, TXD_PIN, RXD_PIN, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE);
    // We won't use a buffer for sending data.
    uart_driver_install(UART_NUM_1, RX_BUF_SIZE * 2, 0, 0, NULL, 0);
}
//===================================================================================================//
xQueueHandle uart_tx_queue = NULL;
xQueueHandle uart_rx_queue = NULL;
//===================================================================================================//
int sendData(const char* logName, const char* data)
{
    const int len = strlen(data);
    const int txBytes = uart_write_bytes(UART_NUM_1, data, len);
    ESP_LOGI(logName, "Wrote %d bytes,string :%s", txBytes, data);
    for(int i=0;i<txBytes;i++)
    {
        printf("data[%d]=0x%x",i,data[i]);
    }
    printf("\n");
    return txBytes;
}

int sendCmd(const char* logName, const char* data, int len)
{
    const int txBytes = uart_write_bytes(UART_NUM_1, data, len);
    ESP_LOGI(logName, "Wrote %d bytes,string :%s", txBytes, data);
    for(int i=0;i<txBytes;i++)
    {
        printf("data[%d]=0x%x  ",i,data[i]);
    }
    printf("\n");
    return txBytes;
}
//===================================================================================================//

//===================================================================================================//
static void tx_task()
{
    int i;
    tx_data.buffer = audio_calloc(1,1024);
    tx_data.data = audio_calloc(1,1024);
    uart_tx_data_format_t utx_data;
    printf("tx task init!\n");
    uart_tx_queue = xQueueCreate(5, sizeof(uart_tx_data_format_t));
    esp_err_t ret;
    static const char *TX_TASK_TAG = "TX_TASK";
    esp_log_level_set(TX_TASK_TAG, ESP_LOG_INFO);
    while (1) 
    {
        ret = xQueueReceive(uart_tx_queue, &utx_data, portMAX_DELAY);
        ESP_LOGI(TX_TASK_TAG,"uart tx task received ,the result is %d",ret);
        for(i=0; i<utx_data.trans_len; i++)
        {
            ESP_LOGI(TAG,"utx send to s3 data[%d]=%d",i,utx_data.buffer[i]);
        }
        if(ret == true)
        {
            
            sendCmd(TX_TASK_TAG, utx_data.buffer, utx_data.trans_len);
        }
    }
    vTaskDelete(NULL);
}

//===================================================================================================//
//
//
//
//===================================================================================================//
static void uart_tx_set_data(uart_tx_data_format_t *data)
{
    char checksum = 0;
    int i;
    data->buffer[0] = 0xFF;
    data->buffer[1] = 0xF1;
    data->buffer[2] = (data->len & 0x00FF);
    data->buffer[3] = (data->len & 0xFF00) >> 8;
    data->buffer[4] =  data->cmd & 0x00FF;
    data->buffer[5] = (data->cmd & 0xFF00) >> 8;
    size_t len = data->len;
    printf("uart_tx_set_data2 2,len is %d\n",len);
    for(i=0; i< len-4; i++)
    {
        data->buffer[i+6] = data->data[i];
    }
    printf("uart_tx_set_data3 3, cmd len is %d\n",data->len);
    for(i=2; i<len+2; i++)
    {
        checksum += data->buffer[i];
    }
    data->buffer[data->len+2] = ~checksum +1;
    data->trans_len = data->len + 3;
    for(i=0; i<data->trans_len; i++)
    {
        printf("uart_tx_set_data4 buffer[%d]=%d\n", i, data->buffer[i]);
    }
}

//===================================================================================================//
bool uart_tx_data(uart_tx_data_format_t *data)
{
    bool ret = false;
    int i;
    if(uart_tx_queue != NULL)
    {   
        uart_tx_set_data(data);
        for(i=0; i<data->trans_len; i++)
        {
            printf("uart_tx_data buffer[%d]=%d\n", i, data->buffer[i]);
        }

        ret = xQueueSend(uart_tx_queue,data,10);
    }
    return ret;
}
//===================================================================================================//
void uart_tx_checksum_fail(void)
{
    uart_tx_data_format_t cmd;
    tx_data.cmd = 0xFF;
    tx_data.len = 4;

    printf("uart_tx_checksum_fail \n");
    uart_tx_data(&tx_data);
    printf("uart_tx_checksum_fail  end\n");
}
//===================================================================================================//

void uart_tx_checksum_suc(uint8_t *data)
{
    uart_tx_data_format_t cmd;
    
    tx_data.cmd = 0xF0;
    tx_data.len = 6;
    tx_data.data[0] = *(data+4);
    tx_data.data[1] = *(data+5);

    printf("uart tx checksum suc data 3 = %d,data 4 = %d\n",tx_data.data[0],tx_data.data[1]);

    uart_tx_data(&tx_data); 
}
//====================================================================================================//
//Function:     Uart RX task
//
//
//
//
//====================================================================================================//
static esp_err_t uart_rx_check_data(uint8_t *buffer,int len)
{
    char checksum = 0;
    int i = 0;
    if(buffer[0] != 0xFF || buffer[1] != 0xF1)
    {
        uart_tx_checksum_fail();
        printf("uart rx check data failed 1!\n");
        return ESP_FAIL;
    }
    for(i=2;i<len-2;i++)
    {
        checksum += buffer[i];
    }
    checksum = ~checksum + 1;
    if(checksum == buffer[len-1])
    {
        printf("uart rx check data suc 1!\n");
        return ESP_OK;
    }
    printf("checksum = %d,buffer[%d] = %d\n",checksum,len-1,buffer[len-1]);
    
    return ESP_FAIL;
}
//==================================================================================================//
static void rx_event(uart_cmd_t cmd)
{
    #if 0
    switch(cmd.cmd)
    {
        case UART_BLE_INIT_M:               //ble master init
            ble_slave_task_deinit();
            ble_master_task_init();
            break;
        case UART_BLE_INIT_S:               //ble slave init
            ble_mask_task_deinit();
            ble_slave_task_init();
            break;
        case UART_WIFI_SET:                 //wifi set up mode
            g_sys.mode = MODE_WIFI_SETUP;
            ble_blufi_task();
            break;
        case UART_WIFI_SET_SUC:             //wifi set suc


            break;
        case UART_FACTORY:                  //

            break;
        case UART_FACTORY_SU:               //

            break;
        case UART_SLEEP:                    //Sleep Mode
            
            break;
        case UART_BLE_CON_M:                //Uart BLE Master Connect suc

            break;
        case UART_BLE_CON_S:                //BLE Slav Connect Suc

            break;
        case UART_GYRO_DATA:                //Transfer Gyro Data

            break;
        case UART_AUDIO_DATA:               //Transfer Audio Data

            break;
        case UART_DATA_0:                   //

            break;
        case UART_DATA_1:                   //

            break;
        case UART_PHOTO:                    //Transfer Photo data
            if(g_sys.mode == MODE_PHOTO)
            {

            }
            else
            {
                ESP_LOGE(TAG,"wrong sys mode,the mode is %d,not mode_photo");
            }
            break;
        case UART_CHECK_SUC:                //Cmd checksum suc
        
            break;
        case UART_CHECK_FAIL:               //Cmd Checksum fail

            break;
        default:
            break;
    }
    #endif
}



//=============================================================================================//
static esp_err_t rx_parse(uint8_t *buffer,int len,uart_cmd_t cmd)
{
    esp_err_t ret;
    ret = uart_rx_check_data(buffer, len);
    if(ret != ESP_OK)
    {
        return ESP_FAIL;
    }
    if(buffer[5] == 0xF0)
    {
        return ESP_OK;
    }
    else if(buffer[5] == 0xFF)                  //
    {
        return ESP_OK;
    }

    cmd.cmd = buffer[5];
    cmd.cmd = (cmd.cmd << 8) | buffer[4];
    memcpy(cmd.data,buffer+5,cmd.len-5);
    cmd.data[cmd.len-4] = 0;

    switch(cmd.cmd)
    {
        case 0x0:
            break;
        case 0x1:
            break;
        case 0x2:
            break;
        case 0x3:
            break;
        case 0x4:
            break;
        case 0x5:
            break;
        case 0x6:
            break;
        case 0x7:
            break;
        case 0x8:
            break;
        case 0x9:
            break;
        case 0xA :
            break;
        case 0xB:
            break;
        case 0xC:
            break;
        default:
            ESP_LOGE(TAG,"uart received the error cmd:%d",cmd.cmd);
            break;
    }
}

//==================================================================================================//
char cmd_ble_master_suc[7] = {0xFF,0xF1,0x04,0x00,0x08,0x00,0xF4};
char cmd_ble_salver_conn_suc[7] = {0xFF,0xF1,0x04,0x00,0x09,0x00,0xF3};
char cmd_gyro_mode[7] = {0xFF,0xF1,0x04,0x00,0x11,0x00,0xEB};
char cmd_wifi_set_mode[7] = {0xFF,0xF1,0x04,0x00,0x12,0x00,0xEA};
char cmd_photo_mode[7] = {0xFF,0xF1,0x04,0x00,13,0x00,0xE9};

//==================================================================================================//
static void rx_task()
{
    esp_err_t ret;
    uart_cmd_t cmd;
    int cnt = 6000;
    int level = 0;
    static const char *TX_TASK_TAG = "TX_TASK";
    cmd.data = audio_calloc(1, 1024);
    static const char *RX_TASK_TAG = "RX_TASK";
    esp_log_level_set(RX_TASK_TAG, ESP_LOG_INFO);
    uint8_t* buf = (uint8_t*) malloc(RX_BUF_SIZE+1);
   //char *data = audio_calloc(1,RX_BUF_SIZE+1);
    while (1) 
    {
        const int rxBytes = uart_read_bytes(UART_NUM_1, buf, RX_BUF_SIZE, 10 / portTICK_RATE_MS);
        if (rxBytes > 0) 
        {
            for(int i=0;i<rxBytes;i++)
            {
                ESP_LOGI(RX_TASK_TAG,"read rx data[%d]=%d",i,buf[i]);
            }

            ret = uart_rx_check_data(buf, rxBytes);
            
            buf[rxBytes] = 0;
            ESP_LOGI(RX_TASK_TAG, "Read %d bytes: '%s',uart rx check data is %d", rxBytes, buf, ret);
            ESP_LOG_BUFFER_HEXDUMP(RX_TASK_TAG, buf, rxBytes, ESP_LOG_INFO);
            if(ret == ESP_OK)
            {
                uart_tx_checksum_suc(buf);
                ESP_LOGE(RX_TASK_TAG,"uart rx data checksum suc!");
                //rx_parse(buf, cmd);
                continue;
            }
        }
        else
        {
            cnt++;
            if(cnt > 6000)
            {
                cnt = 0;

                level++;
                if(level == 0)
                {  
                    sendCmd(TX_TASK_TAG, cmd_ble_master_suc, 7);
                }
                else if(level == 1)
                {
                    sendCmd(TX_TASK_TAG, cmd_ble_salver_conn_suc, 7);
                }
                else if(level == 2)
                {
                    sendCmd(TX_TASK_TAG, cmd_gyro_mode, 7);
                }
                else if(level == 3)
                {
                    sendCmd(TX_TASK_TAG, cmd_wifi_set_mode, 7);
                }
                else if(level == 4)
                {
                    sendCmd(TX_TASK_TAG, cmd_photo_mode, 7);
                }
                else
                {
                    level = 0;
                } 
            }
        }
    }
    free(buf);
    audio_free(cmd.data);
    vTaskDelete(NULL);
}
//=======================================================================================================//




void uart_init(void)
{
    init();
    xTaskCreatePinnedToCore(rx_task, "uart_rx_task", 1024*4, NULL, configMAX_PRIORITIES, NULL,1);
    xTaskCreatePinnedToCore(tx_task, "uart_tx_task", 1024*4, NULL, 5, NULL,1);
}









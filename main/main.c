/* MDNS-SD Query and advertise Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "freertos/queue.h"
#include "esp_system.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "audio_mem.h"
#include "../include/wifi.h"
#include "../include/ble_slaver.h"
#include "../include/uart.h"
#include "../include/main.h"



static const char *TAG = "MAIN_APP";


void app_main(void)
{
    printf("app_main:1\n");
    esp_err_t ret = nvs_flash_init();
    printf("app_main:2\n");
	if(ret==ESP_ERR_NVS_NO_FREE_PAGES)
	{
		ESP_ERROR_CHECK(nvs_flash_erase());
		ret = nvs_flash_init();
	}
	ESP_ERROR_CHECK(ret);
    printf("app_main init !\n");
    initialise_wifi();
    uart_init();
    printf("app_main init end !\n");
}






void delay_ms(int num)
{
    if(num > 0)
    {
        vTaskDelay(num / portTICK_PERIOD_MS);
    }
}



void app_task(void)
{
    g_sys = audio_calloc(1,sizeof(sys_sta));

    g_sys->g_queue = xQueueCreate(5,sizeof(sys_cmd_t));
    if(g_sys->g_queue == NULL)
    {
        ESP_LOGE(TAG,"g_queue calloc mem failed!");
    }
    sys_cmd_t msg;
    bool res;
    while(1)
    {
        res = xQueueReceive(g_sys->g_queue,&msg,portMAX_DELAY);
        switch(msg.cmd)
        {
            case BLE_MASTER_INIT:
                //ble_slaver_deinit();
                //ble_master_init();
                break;
            case BLE_MASTER_CONN:
                //ble_master_deinit();
                //ble_slaver_init();
                break;
            case BLE_MASTER_DISCON:

                break;
            case BLE_MASTER_DEINIT:     

                break;
            case BLE_SLAVER_INIT:

                break;
            case BLE_SALVER_CONN:

                break;
            case BLE_SALVER_DISCON:

                break;
            case BLE_SALVER_DEINIT:
                
                break;
            case RECEIVE_AUDIO_DATA:

                break;
            case RECEIVE_GYRO_DATA:

                break;
            case RECEIVE_PHOTO_DATA:

                break;
            default:
                ESP_LOGE(TAG,"received the wrong msg:%d",msg.cmd);
                break;

        }
    }
}





void sendtoapp(sys_cmd_t cmd)
{
    if(g_sys->g_queue != NULL)
    {
        xQueueSend(g_sys->g_queue,&cmd,10/portTICK_PERIOD_MS);
        ESP_LOGI(TAG,"send the cmd:%d to main app",cmd.cmd);
    }
}




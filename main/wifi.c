/* MDNS-SD Query and advertise Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event_loop.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "mdns.h"
#include "driver/gpio.h"
#include <sys/socket.h>
#include <netdb.h>
#include "esp_pm.h"
#include "lwip/err.h"
#include "lwip/sockets.h"
#include "lwip/sys.h"
#include <lwip/netdb.h>
//#include "esp_websocket_client.h"


#define TCP_SERVER_ENABLE 
//#define WEBSOCKET_ENABLE
/* The examples use simple WiFi configuration that you can set via
   'make menuconfig'.

   If you'd rather not, just change the below entries to strings with
   the config you want - ie #define EXAMPLE_WIFI_SSID "mywifissid"
*/
#define EXAMPLE_WIFI_SSID CONFIG_WIFI_SSID
#define EXAMPLE_WIFI_PASS CONFIG_WIFI_PASSWORD

#define EXAMPLE_MDNS_HOSTNAME CONFIG_MDNS_HOSTNAME
#define EXAMPLE_MDNS_INSTANCE CONFIG_MDNS_INSTANCE


#define DEFAULT_LISTEN_INTERVAL CONFIG_WIFI_LISTEN_INTERVAL
#if CONFIG_POWER_SAVE_MIN_MODEM
#define DEFAULT_PS_MODE WIFI_PS_MIN_MODEM
#elif CONFIG_POWER_SAVE_MAX_MODEM
#define DEFAULT_PS_MODE WIFI_PS_MAX_MODEM
#elif CONFIG_POWER_SAVE_NONE
#define DEFAULT_PS_MODE WIFI_PS_NONE
#else
#define DEFAULT_PS_MODE WIFI_PS_NONE
#endif /*CONFIG_POWER_SAVE_MODEM*/

static const char *NVS_CUSTOMER = "wifi data";
/* FreeRTOS event group to signal when we are connected & ready to make a request */
static EventGroupHandle_t wifi_event_group;




#ifdef WEBSOCKET_ENABLE
static void websocket_app_start(void);
#endif




#ifdef TCP_SERVER_ENABLE
#define CONFIG_EXAMPLE_IPV4
const int IPV4_GOTIP_BIT = BIT0;
const int IPV6_GOTIP_BIT = BIT1;
#define PORT 80
static void wait_for_ip();
static void tcp_server_task(void *pvParameters);
#endif
/* The event group allows multiple bits for each event,
   but we only care about one event - are we connected
   to the AP with an IP? */
const int IP4_CONNECTED_BIT = BIT0;
const int IP6_CONNECTED_BIT = BIT1;

bool flag_ssid_orig = false;
static const char *TAG = "wifi";
static bool auto_reconnect = true;

static esp_err_t event_handler(void *ctx, system_event_t *event)
{
    switch(event->event_id) 
    {
    case SYSTEM_EVENT_STA_START:
        ESP_LOGI(TAG, "SYSTEM_EVENT_STA_START");
        esp_wifi_connect();
        break;
    case SYSTEM_EVENT_STA_CONNECTED:
        /* enable ipv6 */
        tcpip_adapter_create_ip6_linklocal(TCPIP_ADAPTER_IF_STA);
        break;
    case SYSTEM_EVENT_STA_GOT_IP:
        xEventGroupSetBits(wifi_event_group, IP4_CONNECTED_BIT);
        #ifdef TCP_SERVER_ENABLE
        xEventGroupSetBits(wifi_event_group, IPV4_GOTIP_BIT);
        #endif
        break;
    case SYSTEM_EVENT_AP_STA_GOT_IP6:
        xEventGroupSetBits(wifi_event_group, IP6_CONNECTED_BIT);
        ESP_LOGI(TAG, "SYSTEM_EVENT_STA_GOT_IP6");

        char *ip6 = ip6addr_ntoa(&event->event_info.got_ip6.ip6_info.ip);
        ESP_LOGI(TAG, "IPv6: %s", ip6);
        break;
    case SYSTEM_EVENT_STA_DISCONNECTED:
        /* This is a workaround as ESP32 WiFi libs don't currently
           auto-reassociate. */
        if (auto_reconnect) {
            esp_wifi_connect();
        }
        xEventGroupClearBits(wifi_event_group, IP4_CONNECTED_BIT | IP6_CONNECTED_BIT);
        #ifdef TCP_SERVER_ENABLE
        xEventGroupClearBits(wifi_event_group, IPV4_GOTIP_BIT);
        xEventGroupClearBits(wifi_event_group, IPV6_GOTIP_BIT);
        #endif
        break;
    default:
        break;
    }
    mdns_handle_system_event(ctx, event);
    return ESP_OK;
}

void initialise_wifi(void)
{
    size_t len;
    esp_err_t ret;
    nvs_handle my_nvs_handle1;   
     #if CONFIG_PM_ENABLE                                                                    //set the mode is power save mode
    // Configure dynamic frequency scaling: maximum frequency is set in sdkconfig,
    // minimum frequency is XTAL.
    
    esp_pm_config_esp32_t pm_config = {
            .max_freq_mhz = CONFIG_EXAMPLE_MAX_CPU_FREQ_MHZ,
            .min_freq_mhz = CONFIG_EXAMPLE_MIN_CPU_FREQ_MHZ,
            #if CONFIG_FREERTOS_USE_TICKLESS_IDLE
            .light_sleep_enable = true
            #endif
    };

    //ESP_ERROR_CHECK( esp_pm_configure(&pm_config) );
    #endif // CONFIG_PM_ENABLE
    #if CONFIG_FREERTOS_USE_TICKLESS_IDLE
    {
       ESP_LOGI(TAG,"light sleep mode enbale!");
    }
    #endif

    tcpip_adapter_init();
    wifi_event_group = xEventGroupCreate();
    ESP_ERROR_CHECK( esp_event_loop_init(event_handler, NULL) );
    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK( esp_wifi_init(&cfg) );
    ESP_ERROR_CHECK( esp_wifi_set_storage(WIFI_STORAGE_RAM) );
    wifi_config_t wifi_config = {
        .sta = {
            .ssid = EXAMPLE_WIFI_SSID,
            .password = EXAMPLE_WIFI_PASS,
        },
    };

    len = sizeof(wifi_config);
    ESP_LOGI(TAG,"WIFI_CONFIG LENGTH IS %d",len);                                                   //read the wifi config parameter from nvs flash
    ret = nvs_open(NVS_CUSTOMER,NVS_READONLY,&my_nvs_handle1);  
    if(ret != ESP_OK)
    {
        ESP_LOGI(TAG,"NO NVS Flash!");
        flag_ssid_orig = true;
    }
    ret = nvs_get_blob(my_nvs_handle1,"nvs_wifi",&wifi_config,&len);

    if(ret != ESP_OK)
    {
        ESP_LOGI(TAG,"No Wifi Config Info!\r\n");
        flag_ssid_orig = true;
    }
    else
    {
        ESP_LOGI(TAG,"**************************NVS GET BLOB SUCCESS!*****");
    }
    ESP_LOGI(TAG,"nvs_wifi_config     ssid:%s password:%s",wifi_config.sta.ssid,wifi_config.sta.password);
    nvs_close(my_nvs_handle1);
    wifi_config.sta.listen_interval = DEFAULT_LISTEN_INTERVAL;
    printf("Setting WiFi Configuration SSID:%s\n",wifi_config.sta.ssid);
    printf("settting wifi password is:%s\n",wifi_config.sta.password);
    ESP_LOGI(TAG, "Setting WiFi configuration SSID %s...", wifi_config.sta.ssid);
    ESP_ERROR_CHECK( esp_wifi_set_mode(WIFI_MODE_STA) );
    ESP_ERROR_CHECK( esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config) );
    ESP_ERROR_CHECK( esp_wifi_start() );
    ESP_ERROR_CHECK(esp_wifi_set_ps(DEFAULT_PS_MODE));

}

void initialise_mdns(void)
{
    //initialize mDNS
    ESP_ERROR_CHECK( mdns_init() );
    //set mDNS hostname (required if you want to advertise services)
    ESP_ERROR_CHECK( mdns_hostname_set(EXAMPLE_MDNS_HOSTNAME) );
    //set default mDNS instance name
    ESP_ERROR_CHECK( mdns_instance_name_set(EXAMPLE_MDNS_INSTANCE) );

    //structure with TXT records
    mdns_txt_item_t serviceTxtData[3] = {
        {"board","esp32"},
        {"u","user"},
        {"p","password"}
    };
    #if 0
    //initialize service
    ESP_ERROR_CHECK( mdns_service_add("ESP32-WebServer", "_http", "_tcp", 80, serviceTxtData, 3) );
    //add another TXT item
    ESP_ERROR_CHECK( mdns_service_txt_item_set("_http", "_tcp", "path", "/foobar") );
    //change TXT item value
    ESP_ERROR_CHECK( mdns_service_txt_item_set("_http", "_tcp", "u", "admin") );
    #else
    ESP_ERROR_CHECK( mdns_service_add("ESP32-WebServer", "_bellwebsocket", "_tcp", 80, serviceTxtData, 3) );
    ESP_ERROR_CHECK( mdns_service_txt_item_set("_bellwebsocket", "_tcp", "path", "/foobar") );
    ESP_ERROR_CHECK( mdns_service_txt_item_set("_bellwebsocket", "_tcp", "u", "admin") );
    #endif
}

static const char * if_str[] = {"STA", "AP", "ETH", "MAX"};
static const char * ip_protocol_str[] = {"V4", "V6", "MAX"};

static void mdns_print_results(mdns_result_t * results)
{
    mdns_result_t *r = results;
    mdns_ip_addr_t *a = NULL;
    int i = 1, t;
    while(r)
    {
        printf("%d: Interface: %s, Type: %s\n", i++, if_str[r->tcpip_if], ip_protocol_str[r->ip_protocol]);
        if(r->instance_name)
        {
            printf("  PTR : %s\n", r->instance_name);
        }
        if(r->hostname)
        {
            printf("  SRV : %s.local:%u\n", r->hostname, r->port);
        }
        if(r->txt_count)
        {
            printf("  TXT : [%u] ", r->txt_count);
            for(t=0; t<r->txt_count; t++)
            {
                printf("%s=%s; ", r->txt[t].key, r->txt[t].value?r->txt[t].value:"NULL");
            }
            printf("\n");
        }
        a = r->addr;
        while(a)
        {
            if(a->addr.type == IPADDR_TYPE_V6)
            {
                printf("  AAAA: " IPV6STR "\n", IPV62STR(a->addr.u_addr.ip6));
            }
            else 
            {
                printf("  A   : " IPSTR "\n", IP2STR(&(a->addr.u_addr.ip4)));
            }
            a = a->next;
        }
        r = r->next;
    }
}

static void query_mdns_service(const char * service_name, const char * proto)
{
    ESP_LOGI(TAG, "Query PTR: %s.%s.local", service_name, proto);

    mdns_result_t * results = NULL;
    esp_err_t err = mdns_query_ptr(service_name, proto, 3000, 20,  &results);
    if(err){
        ESP_LOGE(TAG, "Query Failed: %s", esp_err_to_name(err));
        return;
    }
    if(!results){
        ESP_LOGW(TAG, "No results found!");
        return;
    }

    mdns_print_results(results);
    mdns_query_results_free(results);
}

static void query_mdns_host(const char * host_name)
{
    ESP_LOGI(TAG, "Query A: %s.local", host_name);

    struct ip4_addr addr;
    addr.addr = 0;

    esp_err_t err = mdns_query_a(host_name, 2000,  &addr);
    if(err){
        if(err == ESP_ERR_NOT_FOUND){
            ESP_LOGW(TAG, "%s: Host was not found!", esp_err_to_name(err));
            return;
        }
        ESP_LOGE(TAG, "Query Failed: %s", esp_err_to_name(err));
        return;
    }

    ESP_LOGI(TAG, IPSTR, IP2STR(&addr));
}

static void initialise_button(void)
{
    gpio_config_t io_conf;
    io_conf.intr_type = GPIO_PIN_INTR_DISABLE;
    //io_conf.pin_bit_mask = 1;
    io_conf.pin_bit_mask = GPIO_SEL_37;
    io_conf.mode = GPIO_MODE_INPUT;
    io_conf.pull_up_en = 1;
    io_conf.pull_down_en = 0;
    gpio_config(&io_conf);
}

static void check_button(void)
{
    static bool old_level = true;
    //bool new_level = gpio_get_level(GPIO_NUM_0);
    bool new_level = gpio_get_level(GPIO_NUM_37);
    if (!new_level && old_level) {
        query_mdns_host("esp32");
        query_mdns_service("_arduino", "_tcp");
        query_mdns_service("_http", "_tcp");
        query_mdns_service("_bellwebsocket", "_tcp");
        query_mdns_service("_printer", "_tcp");
        query_mdns_service("_ipp", "_tcp");
        query_mdns_service("_afpovertcp", "_tcp");
        query_mdns_service("_smb", "_tcp");
        query_mdns_service("_ftp", "_tcp");
        query_mdns_service("_nfs", "_tcp");
    }
    old_level = new_level;
}

static void mdns_example_task(void *pvParameters)
{
    /* Wait for the callback to set the CONNECTED_BIT in the event group. */
    xEventGroupWaitBits(wifi_event_group, IP4_CONNECTED_BIT | IP6_CONNECTED_BIT,
                     false, true, portMAX_DELAY);
    while(1) {
        //check_button();
        vTaskDelay(50 / portTICK_PERIOD_MS);
    }
}














#ifdef WEBSOCKET_ENABLE

#define NO_DATA_TIMEOUT_SEC     100
//#define CONFIG_WEBSOCKET_URI    "ws://esp32-mdns-2.local"
#define  CONFIG_WEBSOCKET_URI   "ws://127.0.0.1:8800"
//#define CONFIG_WEBSOCKET_URI    "ws://echo.websocket.org"  
//#define CONFIG_WEBSOCKET_URI    "ws://localhost:8080"
//#define  CONFIG_WEBSOCKET_URI   "ws://192.168.31.85:5353"
static const char *WEBTAG = "WEBSOCKET";

static EventGroupHandle_t wifi_event_group;
const static int CONNECTED_BIT = BIT0;

static TimerHandle_t shutdown_signal_timer;
static SemaphoreHandle_t shutdown_sema;

static void shutdown_signaler(TimerHandle_t xTimer)
{
    ESP_LOGI(WEBTAG, "No data received for %d seconds, signaling shutdown", NO_DATA_TIMEOUT_SEC);
    xSemaphoreGive(shutdown_sema);
}

#if CONFIG_WEBSOCKET_URI_FROM_STDIN
static void get_string(char *line, size_t size)
{
    int count = 0;
    while (count < size) {
        int c = fgetc(stdin);
        if (c == '\n') {
            line[count] = '\0';
            break;
        } else if (c > 0 && c < 127) {
            line[count] = c;
            ++count;
        }
        vTaskDelay(10 / portTICK_PERIOD_MS);
    }
}

#endif /* CONFIG_WEBSOCKET_URI_FROM_STDIN */

static void websocket_event_handler(void *handler_args, esp_event_base_t base, int32_t event_id, void *event_data)
{
    esp_websocket_event_data_t *data = (esp_websocket_event_data_t *)event_data;
    switch (event_id) {
    case WEBSOCKET_EVENT_CONNECTED:
        ESP_LOGI(WEBTAG, "WEBSOCKET_EVENT_CONNECTED");
        break;
    case WEBSOCKET_EVENT_DISCONNECTED:
        ESP_LOGI(WEBTAG, "WEBSOCKET_EVENT_DISCONNECTED");
        break;
    case WEBSOCKET_EVENT_DATA:
        ESP_LOGI(WEBTAG, "WEBSOCKET_EVENT_DATA");
        ESP_LOGI(WEBTAG, "Received opcode=%d", data->op_code);
        ESP_LOGW(WEBTAG, "Received=%.*s", data->data_len, (char *)data->data_ptr);
        ESP_LOGW(WEBTAG, "Total payload length=%d, data_len=%d, current payload offset=%d\r\n", data->payload_len, data->data_len, data->payload_offset);

        xTimerReset(shutdown_signal_timer, portMAX_DELAY);
        break;
    case WEBSOCKET_EVENT_ERROR:
        ESP_LOGI(WEBTAG, "WEBSOCKET_EVENT_ERROR");
        break;
    }
}

static void websocket_app_start(void)
{
    esp_websocket_client_config_t websocket_cfg = {};

    shutdown_signal_timer = xTimerCreate("Websocket shutdown timer", NO_DATA_TIMEOUT_SEC * 1000 / portTICK_PERIOD_MS,
                                         pdFALSE, NULL, shutdown_signaler);
    shutdown_sema = xSemaphoreCreateBinary();

#if CONFIG_WEBSOCKET_URI_FROM_STDIN
    char line[128];

    ESP_LOGI(  WEBTAG, "Please enter uri of websocket endpoint");
    get_string(line, sizeof(line));

    websocket_cfg.uri = line;
    ESP_LOGI(WEBTAG, "Endpoint uri: %s\n", line);

#else
    websocket_cfg.uri = CONFIG_WEBSOCKET_URI;

#endif /* CONFIG_WEBSOCKET_URI_FROM_STDIN */

    ESP_LOGI(WEBTAG, "Connecting to %s...", websocket_cfg.uri);
    websocket_cfg.transport = WEBSOCKET_TRANSPORT_OVER_TCP;


    esp_websocket_client_handle_t client = esp_websocket_client_init(&websocket_cfg);
    esp_websocket_register_events(client, WEBSOCKET_EVENT_ANY, websocket_event_handler, (void *)client);

    esp_websocket_client_start(client);
    xTimerStart(shutdown_signal_timer, portMAX_DELAY);
    char data[32];
    int i = 0;
    while (i < 10) 
    {
        if (esp_websocket_client_is_connected(client)) 
        {
            int len = sprintf(data, "hello %04d", i++);
            ESP_LOGI(WEBTAG, "Sending %s", data);
            esp_websocket_client_send(client, data, len, portMAX_DELAY);
        }
        vTaskDelay(1000 / portTICK_RATE_MS);
    }

    xSemaphoreTake(shutdown_sema, portMAX_DELAY);
    esp_websocket_client_stop(client);
    ESP_LOGI(WEBTAG, "Websocket Stopped");
    esp_websocket_client_destroy(client);
}
#endif








#ifdef TCP_SERVER_ENABLE

#define TCP_SERVER "TCP"

static void wait_for_ip()
{
    uint32_t bits = IPV4_GOTIP_BIT | IPV6_GOTIP_BIT ;

    ESP_LOGI(TCP_SERVER, "Waiting for AP connection...");
    xEventGroupWaitBits(wifi_event_group, bits, false, true, portMAX_DELAY);
    ESP_LOGI(TCP_SERVER, "Connected to AP");
}

static void tcp_server_task(void *pvParameters)
{
    char rx_buffer[1024];
    char addr_str[128];
    int addr_family;
    int ip_protocol;

    while (1) 
    {

#ifdef CONFIG_EXAMPLE_IPV4
        struct sockaddr_in destAddr;
        destAddr.sin_addr.s_addr = htonl(INADDR_ANY);
        destAddr.sin_family = AF_INET;
        destAddr.sin_port = htons(PORT);
        addr_family = AF_INET;
        ip_protocol = IPPROTO_IP;
        inet_ntoa_r(destAddr.sin_addr, addr_str, sizeof(addr_str) - 1);
#else // IPV6
        struct sockaddr_in6 destAddr;
        bzero(&destAddr.sin6_addr.un, sizeof(destAddr.sin6_addr.un));
        destAddr.sin6_family = AF_INET6;
        destAddr.sin6_port = htons(PORT);
        addr_family = AF_INET6;
        ip_protocol = IPPROTO_IPV6;
        inet6_ntoa_r(destAddr.sin6_addr, addr_str, sizeof(addr_str) - 1);
#endif

        int listen_sock = socket(addr_family, SOCK_STREAM | O_NONBLOCK, ip_protocol);
        if (listen_sock < 0) 
        {
            ESP_LOGE(TCP_SERVER, "Unable to create socket: errno %d", errno);
            break;
        }
        //======//
        int flags = fcntl(listen_sock, F_GETFL, 0);
        fcntl(listen_sock, F_SETFL, flags | O_NONBLOCK);

        //=====//set the sock is NONBLOCK state
        ESP_LOGI(TCP_SERVER, "Socket created");
        

        int err = bind(listen_sock, (struct sockaddr *)&destAddr, sizeof(destAddr));
        if (err != 0) 
        {
            ESP_LOGE(TCP_SERVER, "Socket unable to bind: errno %d", errno);
            break;
        }
        ESP_LOGI(TCP_SERVER, "Socket binded");

        err = listen(listen_sock, 1);
        if (err != 0) {
            ESP_LOGE(TCP_SERVER, "Error occured during listen: errno %d", errno);
            break;
        }
        ESP_LOGI(TCP_SERVER, "Socket listening");

        struct sockaddr_in6 sourceAddr; // Large enough for both IPv4 or IPv6
        uint addrLen = sizeof(sourceAddr);
        int sock = accept(listen_sock, (struct sockaddr *)&sourceAddr, &addrLen);
        if (sock < 0) {
            ESP_LOGE(TCP_SERVER, "Unable to accept connection: errno %d", errno);
            break;
        }
        ESP_LOGI(TCP_SERVER, "Socket accepted");

        int cnt = 1;
        while(cnt++)
        {
            vTaskDelay(2000/portTICK_PERIOD_MS);
            
            bzero(rx_buffer,20);
            sprintf(rx_buffer,"this is a book %d!",cnt);
            //strcat(rx_buffer,"this is a book!");

            int error = send(sock, rx_buffer, 20, 0);
            if (error < 0) 
            {
                ESP_LOGE( TCP_SERVER, "Error occured during sending: errno %d", errno);
                break;
            }
            ESP_LOGI(TAG,"\t\t %d:send the data to ap\n",cnt);
            if(cnt > 10)
            {
                break;
            }
        }

        while (1) 
        {
            int len = recv(sock, rx_buffer, sizeof(rx_buffer) - 1, 0);
            // Error occured during receiving
            if (len < 0) 
            {
                ESP_LOGE(TCP_SERVER, "recv failed: errno %d", errno);
                break;
            }
            // Connection closed
            else if (len == 0) 
            {
                ESP_LOGI(TCP_SERVER, "Connection closed");
                break;
            }
            // Data received
            else 
            {
                ESP_LOGI(TCP_SERVER,"tcp recv the data len is %d",len);
                // Get the sender's ip address as string
                if (sourceAddr.sin6_family == PF_INET) 
                {
                    inet_ntoa_r(((struct sockaddr_in *)&sourceAddr)->sin_addr.s_addr, addr_str, sizeof(addr_str) - 1);
                } 
                else if (sourceAddr.sin6_family == PF_INET6) 
                {
                    inet6_ntoa_r(sourceAddr.sin6_addr, addr_str, sizeof(addr_str) - 1);
                }

                rx_buffer[len] = 0;                                                     // Null-terminate whatever we received and treat like a string
                ESP_LOGI(TCP_SERVER, "Received %d bytes from %s:", len, addr_str);
                ESP_LOGI(TCP_SERVER, "%s", rx_buffer); 
                int err = send(sock, rx_buffer, len, 0);
                if (err < 0) 
                {
                    ESP_LOGE( TCP_SERVER, "Error occured during sending: errno %d", errno);
                    break;
                }
            }
        }

        if (sock != -1) {
            ESP_LOGE(TCP_SERVER, "Shutting down socket and restarting...");
            shutdown(sock, 0);
            close(sock);
            close(listen_sock);
            vTaskDelay(100 / portTICK_PERIOD_MS);
        }
    }
    vTaskDelete(NULL);
}


#endif
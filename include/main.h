/* GPIO Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

#ifndef _MAIN_H_
#define _MAIN_H_


typedef struct sys_cmd
{
   int cmd;
   char *data;
}sys_cmd_t;

typedef struct uart_cmd
{
   int cmd;
   char *data;
   int len;
}uart_cmd_t;

typedef struct
{
   bool is_ble_on;
   bool is_ble_m;
   bool is_ble_search;
   bool is_ble_con;
}ble_stat_t;

typedef struct 
{
   bool is_wifi_on;
   bool is_wifi_con;
}wifi_stat_t;


typedef enum
{
   BLE_MASTER_INIT,
   BLE_MASTER_CONN,
   BLE_MASTER_DISCON,
   BLE_MASTER_DEINIT,
   BLE_SLAVER_INIT,
   BLE_SALVER_CONN,
   BLE_SALVER_DISCON,
   BLE_SALVER_DEINIT,
   RECEIVE_AUDIO_DATA,
   RECEIVE_GYRO_DATA,
   RECEIVE_PHOTO_DATA,
}enum_cmd_t;



typedef enum
{
   MODE_IDLE = 0x00,
   MODE_WIFI_SETUP = 0x01,
   MODE_PHOTO = 0x02,
   MODE_GYRO = 0x03,
}sys_mode_t;

typedef struct
{
   wifi_stat_t wifi_stat;
   ble_stat_t ble_stat;
   xQueueHandle g_queue;
   sys_mode_t mode;
}sys_sta;

typedef sys_sta *sys_stat_t;
sys_stat_t g_sys;



void delay_ms(int num);

#endif
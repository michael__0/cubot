/* GPIO Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

#ifndef _UART_H_
#define _UART_H_

void uart_init(void);

typedef enum
{
   UART_BLE_INIT_M   = 0x0100,
   UART_BLE_INIT_S   = 0x0200,
   UART_WIFI_SET     = 0x0300,
   UART_WIFI_SET_SUC = 0x0400,
   UART_FACTORY      = 0x0500,
   UART_FACTORY_SU   = 0x0600,
   UART_SLEEP        = 0x0700,
   UART_BLE_CON_M    = 0x0800,
   UART_BLE_CON_S    = 0x0900,
   UART_GYRO_DATA    = 0x0A00,
   UART_AUDIO_DATA   = 0x0B00,
   UART_DATA_0       = 0x0C00,
   UART_DATA_1       = 0x0D00,
   UART_PHOTO        = 0x1000,
   UART_CHECK_SUC    = 0xF000,
   UART_CHECK_FAIL   = 0xFF00,
}uart_msg_t;



#endif